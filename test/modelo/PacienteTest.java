/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import modelo.Paciente;
import modelo.Paciente.Planta;
import java.util.Arrays;
import java.util.Collection;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

/**
 *
 * @author nachorod
 */
@RunWith(Parameterized.class)
public class PacienteTest {
    private String codPaciente;
    private String nomPaciente;
    private String dias;
    private Paciente.Planta planta;
    private int resultado;

    public PacienteTest(String codPaciente, String nomPaciente, String dias, Paciente.Planta planta, int resultado) {
        this.codPaciente = codPaciente;
        this.nomPaciente = nomPaciente;
        this.dias = dias;
        this.planta = planta;
        this.resultado=resultado;
    }

@Parameterized.Parameters
    public static Collection<Object[]> numeros() {
        return Arrays.asList(new Object[][]{
            {"00020", "Fernando", "00020", Planta.geriatria, 0},
            {"00020", "Fernando", "00040", Planta.geriatria, 5},
            {"00020", "Fernando", "00070", Planta.geriatria, 7},
            {"00020", "Fernando", "000100", Planta.geriatria, 10},
            {"00020", "Fernando", "000200", Planta.geriatria, 15},
            {"00020", "Fernando", "00020", Planta.pediatria, 0},
            {"00020", "Fernando", "00050", Planta.pediatria, 5},
            {"00020", "Fernando", "00070", Planta.pediatria, 7},
            {"00020", "Fernando", "00110", Planta.pediatria, 10},
            {"00020", "Fernando", "00250", Planta.pediatria, 15},
            {"00020", "Fernando", "00020", Planta.digestivo, 0},
            {"00020", "Fernando", "00050", Planta.digestivo, 5},
            {"00020", "Fernando", "00070", Planta.digestivo, 7},
            {"00020", "Fernando", "00110", Planta.digestivo, 10},
            {"00020", "Fernando", "00250", Planta.digestivo, 15}
        });    
    }

    /**
     * Test of devuelveBonificacion method, of class Paciente.
     */
    @Test
    public void testDevuelveBonificacion() {
        System.out.println("devuelveBonificacion");
        assertEquals(this.resultado,new Paciente(this.codPaciente,this.nomPaciente, this.dias, this.planta).devuelveBonificacion());
    }
    
}
