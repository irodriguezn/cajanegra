/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import modelo.Paciente;
import modelo.Paciente.Planta;
import vista.Vista;
import vista.VistaPaciente;

/**
 *
 * @author lliurex
 */
public class AppPaciente {
    public static void main(String[] args) {
        Vista v=new Vista();
        VistaPaciente vp=new VistaPaciente();
        Paciente p=vp.leerPaciente();
        vp.mostrarPaciente(p);
        int bonificacion=p.devuelveBonificacion();
        v.mostrarMensaje(bonificacion+"");
    }

}
