/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;


/**
 *
 * @author lliurex
 */
public class Paciente {
    private String codPaciente;
    private String nomPaciente;
    private String dias;
    private Planta planta;

    public Paciente(String codPaciente, String nomPaciente, String dias, Planta planta) {
        this.codPaciente = codPaciente;
        this.nomPaciente = nomPaciente;
        this.dias = dias;
        this.planta = planta;
    }

    public String getCodPaciente() {
        return codPaciente;
    }

    public void setCodPaciente(String codPaciente) {
        this.codPaciente = codPaciente;
    }

    public String getNomPaciente() {
        return nomPaciente;
    }

    public void setNomPaciente(String nomPaciente) {
        this.nomPaciente = nomPaciente;
    }

    public String getDias() {
        return dias;
    }

    public void setDias(String dias) {
        this.dias = dias;
    }

    public Planta getPlanta() {
        return planta;
    }

    public void setPlanta(Planta planta) {
        this.planta = planta;
    }
    
    public int devuelveBonificacion() {
        int bonificacion;
        int d=Integer.parseInt(dias);
        if (d<30) {
            bonificacion=0;
        } else if (d<60) {
            bonificacion=5;
        } else if (d<90) {
            bonificacion=7;
        } else if (d<120) {
            bonificacion=10;
        } else {
            bonificacion=15;
        }
        return bonificacion;
    }

    @Override
    public String toString() {
        return "Paciente{" + "codPaciente=" + codPaciente + ", nomPaciente=" + nomPaciente + ", dias=" + dias + ", planta=" + planta + '}';
    }
    
    
    public enum Planta {geriatria, pediatria, digestivo,
    cardiologia, traumatologia, ginecologia};
}
