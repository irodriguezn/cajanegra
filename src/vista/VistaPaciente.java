/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package vista;

import java.util.Scanner;
import modelo.Paciente;
import modelo.Paciente.Planta;

/**
 *
 * @author nachorod
 */
public class VistaPaciente {
    Vista v=new Vista();
    public Paciente leerPaciente() {
        Scanner sc = new Scanner(System.in);
        String codPaciente=v.validarDigitos(5, "Código (5 dígitos): ");
        String nombre=v.validarDigitos(8, "Nombre (8 caracteres): ");
        System.out.println("Dias ingreso: ");
        String dias=sc.nextLine();
        System.out.println("Planta: ");
        Planta planta=obtienePlanta(sc.nextLine());
        return new Paciente(codPaciente, nombre, dias, planta);
    }
    
    public void mostrarPaciente(Paciente p) {
        System.out.println(p.toString());
    }
    
    static Planta obtienePlanta(String planta) {
        Planta p=null;
        switch (planta) {
            case "cardiologia":
                p=Planta.cardiologia;
                break;
            case "digestivo":
                p=Planta.digestivo;
                break;
            case "geriatria":
                p=Planta.geriatria;
                break;
            case "ginecologia":
                p=Planta.ginecologia;
                break;
            case "pediatria":
                p=Planta.pediatria;
                break;
            case "traumatologia":
                p=Planta.traumatologia;
                break;
        }
        return p;
    }
}
