/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package vista;

import java.util.Scanner;

/**
 *
 * @author nachorod
 */
public class Vista {
    public void mostrarMensaje(String msj) {
        System.out.println(msj);
    }
    
    public String validarDigitos(int numDigitos, String msj) {
        boolean esValido=false;
        String digitos="";
        Scanner sc = new Scanner(System.in);
        while (!esValido) {
            System.out.print(msj);
            digitos=sc.nextLine();
            if (digitos.length()==numDigitos) {
                if (sonDigitos(digitos)) {
                    esValido=true;
                }
            }
        }
        return digitos;
    }
    
    static boolean sonDigitos(String digitos) {
        int digito; //48-57 códigos ASCII de los dígitos
        boolean esDigito=true;
        for (int i=0; i<digitos.length(); i++) {
            digito=digitos.charAt(i);
            if (digito<48 || digito>57) {
                esDigito=false;
                break;
            }
        }
        return esDigito;
    }
}
